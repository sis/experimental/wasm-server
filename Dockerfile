FROM library/rust:latest as build
RUN rustup target add wasm32-wasi
COPY . /usr/src/
WORKDIR /usr/src/
RUN cargo install --target wasm32-wasi --root /usr/ --path .

FROM scratch
COPY --from=build /usr/bin/wasm-server.wasm /
CMD ["/wasm-server.wasm"]
#ENTRYPOINT ["/bin/sola-server"]
