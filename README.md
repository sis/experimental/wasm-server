# wasm-server

Proof of concept of a wasm container.

# Build

```sh
rustup target add wasm32-wasi
cargo build --target wasm32-wasi --release
```

# Build the container (includes a build)

```sh
podman build --annotation "module.wasm.image/variant=compat-smart" -t wasm-server .
```


# Run as a container

This needs [`crun` built with wasm support](https://wasmedge.org/docs/develop/deploy/podman/).

```sh
podman run --rm -it --annotation module.wasm.image/variant=compat-smart wasm-server
```
